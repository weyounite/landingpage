import axios from "axios";
// import router from "@/router"
let BaseApi = axios.create({
  baseURL: "https://iprovide.weyounite.test"
});

//
// BaseApi.interceptors.response.use(
//     response => response,
//     error => {
//       const { status } = error.response
//
//       // Handle Session Timeouts
//       if (status === 401) {
//         alert('session timeout')
//       }
//
//       // Handle Forbidden
//       if (status === 403) {
//         router.push({ name: '403' })
//       }
//
//       // Handle Token Timeouts
//       if (status === 419) {
//           alert('token expired, need new one')
//       }
//
//       return Promise.reject(error)
//     }
// )
BaseApi.defaults.withCredentials = true;

let Api = function() {
  let token = localStorage.getItem("token");
  let csrf = localStorage.getItem('csrf');

  if (token) {
    BaseApi.defaults.headers.common["Authorization"] = `Bearer ${token}`;
  } else {
    BaseApi.defaults.headers.common["Authorization"] = null;
  }

  if (csrf) {
    BaseApi.defaults.headers['X-XSRF-TOKEN'] = csrf
  }

  return BaseApi;
};

export default Api;
