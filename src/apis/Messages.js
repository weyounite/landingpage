import Api from "./Api";
import Csrf from "./Csrf";

export default {
    async index() {
        await Csrf.getCookie();

        return Api().get("/api/messages")
    },

    async detail(id) {
        await Csrf.getCookie();

        return Api().get(`/api/message/${id}`)
    },

    async send(to, message) {
        await Csrf.getCookie();
        return Api().post(`/api/messages/send/${to}`, {message})
    },

    async mark(id) {
        await Csrf.getCookie();

        return Api().put(`api/messages/mark_as_read/${id}`)
    }
};
