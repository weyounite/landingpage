import Api from "./Api";
import Csrf from "./Csrf";

export default {
  async search({lat, lng}) {
    await Csrf.getCookie();
    let radius = 10
    return Api().post("/api/projects/search", {
      lat, lng, radius
    })
  },

  async index() {
    await Csrf.getCookie();

    return Api().get("/api/projects");
  },

  async detail(id) {
    await Csrf.getCookie();

    return Api().get(`/api/projects/${id}`);
  }
};
