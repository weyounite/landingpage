import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import axios from 'axios'
import VueAxios from 'vue-axios'
import lodash from 'lodash'
import Helper from "./mixins/Helper"
import moment from "moment"

// Import libs

import './assets/tailwind.css'
import * as VueGoogleMaps from 'vue2-google-maps'

// configure axios
Vue.use(VueAxios, axios)

Vue.config.productionTip = true

Vue.prototype.$_ = lodash
Vue.prototype.moment = moment

Vue.component('container', require('./components/Container').default)
Vue.component('loading-view', require('./components/LoadingView').default)
Vue.component('loader', require('./components/Icons/Loader').default)
Vue.component('modal', require('./components/Modal').default)

Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyAAeJ4YSOFFouB9r21w1LKw6o3P0Kzm91M',
    libraries: 'places',
  },
})

Vue.mixin(Helper)

new Vue({
  router,
  store,
  created () {
    const userInfo = localStorage.getItem('user')
    if (userInfo) {
      const userData = JSON.parse(userInfo)
      this.$store.commit('setUserData', userData)
    }
    axios.interceptors.response.use(
      response => response,
      error => {
        if (error.response.status === 401) {
          this.$store.dispatch('logout')
        }
        return Promise.reject(error)
      }
    )
  },
  render: h => h(App)
}).$mount('#app')
