import Vue from 'vue'
import Vuex from 'vuex'
import User from "../apis/User";
import Cookie from "js-cookie";

Vue.use(Vuex)


export default new Vuex.Store({
  state: {
    user: null
  },

  mutations: {
    setUserData (state, userData) {
      state.user = userData
      localStorage.setItem('user', JSON.stringify(userData))
      localStorage.setItem("token", userData);
    },

    clearUserData () {
      localStorage.removeItem('token')
      localStorage.removeItem('user')
      Cookie.remove('laravel_session')
      Cookie.remove('XSRF-TOKEN')
      location.reload()
    }
  },

  actions: {
    login ({ commit }, credentials) {
        return User.login(credentials).then(({ data }) => {
            commit('setUserData', data)
        })
        
    },

    logout ({ commit }) {
      User.logout().then(() => {
        commit('clearUserData')
      })

    }
  },

  getters : {
    isLogged: state => !!state.user
  }
})
