import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home'
import Map from '../views/Map.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/account',
    name: 'MyAccount',
    component: () => import(/* webpackChunkName: "projectIndex" */ '../views/MyAccount.vue'),
    meta: {
      authOnly: true
    }
  },
  {
    path: '/projects',
    name: 'ProjectIndex',
    component: () => import(/* webpackChunkName: "projectIndex" */ '../views/ProjectIndex.vue'),
    meta: {
      authOnly: true
    }
  },
  {
    path: '/projects/:project',
    name: 'ProjectDetail',
    component: () => import(/* webpackChunkName: "projectDetail" */ '../views/ProjectDetail.vue'),
    meta: {
      authOnly: true
    },
    props(route) {
      const props = { ...route.params }
      // force projectId as Number
      props.projectId = +props.project
      return props
    },
  },
  {
    path: '/imprint',
    name: 'Impressum',
    component: () => import(/* webpackChunkName: "imprint" */ '../views/Imprint.vue')
  },
  {
    path: '/policy',
    name: 'Nutzungsbedingung',
    component: () => import(/* webpackChunkName: "policy" */ '../views/Policy.vue')
  },
  {
    path: '/privacy',
    name: 'Datenschutz',
    component: () => import(/* webpackChunkName: "privacy" */ '../views/Privacy.vue')
  },
  {
    path: '/map',
    name: 'Map',
    component: Map
  },
  {
    path: '/search',
    name: 'Search',
    component: () => import(/* webpackChunkName: "search" */ '../components/SearchProject')
  },
  {
    path: '*',
    component: () => import(/* webpackChunkName: "404" */ '../views/404')
  }
]

const router = new VueRouter({
  mode: "history",
  routes
});

function isLoggedIn() {
  return localStorage.getItem("auth");
}

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.authOnly)) {
    // this route requires auth, check if logged in
    // if not, redirect to login page.
    if (!isLoggedIn()) {
      next({
        path: "/login",
        query: { redirect: to.fullPath }
      });
    } else {
      next();
    }
  } else if (to.matched.some(record => record.meta.guestOnly)) {
    // this route requires auth, check if logged in
    // if not, redirect to login page.
    if (isLoggedIn()) {
      next({
        path: "/dashboard",
        query: { redirect: to.fullPath }
      });
    } else {
      next();
    }
  } else {
    next(); // make sure to always call next()!
  }
});

export default router
