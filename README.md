# weyounite_lp

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).


### Colors
Green brand: #397D78
Gray text: #000;
light green bg: #C7EBE8
light green background: #CEEAE8
text colour: #2c3e50
gray bg: #EFEFEF